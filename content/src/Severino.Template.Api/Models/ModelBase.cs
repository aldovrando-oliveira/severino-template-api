using System;

namespace Severino.Template.Api.Models
{
    /// <summary>
    /// Classe base para os objtos que farão parte do modelo de dados
    /// </summary>
    public class ModelBase : IModel
    {
        /// <inheritdoc />
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <inheritdoc />
        public DateTimeOffset CreatedAt { get; set; } = DateTimeOffset.Now;

        /// <inheritdoc />
        public DateTimeOffset UpdatedAt { get; set; } = DateTimeOffset.Now;
    }
}