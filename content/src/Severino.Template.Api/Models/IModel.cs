using System;

namespace Severino.Template.Api.Models
{
    /// <summary>
    /// Assinatura para os objetos que farão parte do modelo de dados
    /// </summary>
    public interface IModel
    {
        /// <summary>
        /// Código da entidade
        /// </summary>
        Guid Id { get; set; }
        
        /// <summary>
        /// Data e hora da criação da entidade
        /// </summary>
        DateTimeOffset CreatedAt { get; set; }
        
        /// <summary>
        /// Data e hora da última atualização da entidade
        /// </summary>
        DateTimeOffset UpdatedAt { get; set; }
    }
}