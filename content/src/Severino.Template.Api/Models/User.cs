namespace Severino.Template.Api.Models
{
    /// <summary>
    /// Usuário que utiliza o sistema
    /// </summary>
    public class User : ModelBase
    {
        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Email de login do usuário
        /// </summary>
        public string Email { get; set; }
    }
}