using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Severino.Template.Api.Infra.Exceptions;
using Severino.Template.Api.Models;

namespace Severino.Template.Api.Business
{
    /// <summary>
    /// Classe com as regras de negócio para usuários
    /// </summary>
    public class UserBusiness
    {
        private readonly IRepository<User> _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Cria uma nova instância da classe <see cref="UserBusiness"/>
        /// </summary>
        /// <param name="unitOfWork"></param>
        public UserBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _userRepository = unitOfWork.GetRepository<User>();
        }

        /// <summary>
        /// Cria um novo usuário
        /// </summary>
        /// <param name="user">Usuário que será criado</param>
        /// <returns>Objeto com as informações do usuário criado</returns>
        public async Task<User> CreateAsync(User user)
        {
            await _userRepository.InsertAsync(user);
            await _unitOfWork.SaveChangesAsync();

            return await _userRepository.GetFirstOrDefaultAsync(predicate: x => x.Id == user.Id);
        }

        /// <summary>
        /// Consulta usuário pelo código de identificação
        /// </summary>
        /// <param name="id">Código do usuário</param>
        /// <returns>Objeto com as informações do usuário encontrado</returns>
        /// <exception cref="EntityNotFoundException">Quando o usuário não é encontrado</exception>
        public async Task<User> GetByIdAsync(Guid id)
        {
            var user = await _userRepository.GetFirstOrDefaultAsync(predicate: x => x.Id == id);

            if (user == null)
                throw new EntityNotFoundException("User");

            return user;
        }

        /// <summary>
        /// Atualiza as informações do usuário
        /// </summary>
        /// <param name="id">Código do usuário</param>
        /// <param name="user">Informações do usuário que serão atualizadas</param>
        /// <returns>Objeto com as informações atualizadas do usuário</returns>
        /// <exception cref="EntityNotFoundException">Quando o usuário não é encontrado</exception>
        public async Task<User> UpdateAsync(Guid id, User user)
        {
            var dbUser = await GetByIdAsync(id);

            dbUser.Name = user.Name;
            dbUser.Email = user.Email;
            dbUser.UpdatedAt = DateTimeOffset.Now;

            _userRepository.Update(dbUser);
            await _unitOfWork.SaveChangesAsync();

            return dbUser;
        }

        /// <summary>
        /// Exclui um usuário
        /// </summary>
        /// <param name="id">Código do usuário</param>
        /// <exception cref="EntityNotFoundException">Quando o usuário não é encontrado</exception>
        public async Task DeleteAsync(Guid id)
        {
            var dbUser = await GetByIdAsync(id);

            _userRepository.Delete(dbUser);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}