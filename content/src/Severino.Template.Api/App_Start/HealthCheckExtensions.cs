using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Severino.Template.Api.Infra.Configurations;

namespace Severino.Template.Api
{
    /// <summary>
    /// Extensões para adicionar e configurar healthcheck na aplicação
    /// </summary>
    public static class HealthCheckExtensions
    {
        /// <summary>
        /// Extensão da interface <see cref="IServiceCollection"/> para adicionar healthcheck no container de injeção de dependências
        /// </summary>
        /// <param name="services">Coleção com os objetos que serão gerenciados pelo container de injeção de dependência</param>
        /// <param name="configuration">Objeto com as configurações de ambiente e da aplicação</param>
        public static void AddAppHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHealthChecks()
                .AddMySql(
                    configuration.GetMySqlConnectionString("Template", true),
                    "templatedb",
                    HealthStatus.Unhealthy,
                    new[] {"db", "mysql"});
        }

        /// <summary>
        /// Define os endpoints que são utilizados para monitor a saúde da aplicação
        /// </summary>
        /// <param name="app">Builder de configuração da aplicação</param>
        public static void UseAppHealthCheck(this IApplicationBuilder app)
        {
            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                Predicate = _ => true,
                ResponseWriter = WriteResponseHealthCheck                
            });

            app.UseHealthChecks("/ping", new HealthCheckOptions
            {
                Predicate = entry => !EnumerableExtensions.Any(entry.Tags),
                ResponseWriter = WriteResponseHealthCheck
            });
        }

        private static async Task WriteResponseHealthCheck(HttpContext context, HealthReport report)
        {
            var response = JsonConvert.SerializeObject(new
            {
                Status = report.Status.ToString(),
                report.TotalDuration,
                entries = report.Entries.Select(item => new
                {
                    Name = item.Key,
                    item.Value.Data,
                    Status = item.Value.Status.ToString(),
                    item.Value.Duration,
                    Description = item.Value.Description ?? item.Value.Exception?.Message,
                    Exception = item.Value.Exception?.StackTrace
                })
            }, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            context.Response.ContentType = MediaTypeNames.Application.Json;
            await context.Response.WriteAsync(response);
        }
    }
}