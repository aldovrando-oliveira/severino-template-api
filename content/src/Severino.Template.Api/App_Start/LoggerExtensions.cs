using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NetStash.Extensions.Logging;
using Severino.Template.Api.Infra.Configurations;

namespace Severino.Template.Api
{
    /// <summary>
    /// Extensões para configuração e utilização de Log na aplicação
    /// </summary>
    public static class LoggerExtensions
    {
        /// <summary>
        /// Adiciona configuração de Log no motor de injeção de dependência
        /// </summary>
        /// <param name="services">Objeto para a parametrização da injeção de dependência</param>
        /// <param name="configuration">Objeto com as configurações do ambiente</param>
        /// <param name="useConfigurationFile">Indica se deve ser utilizado arquivo de configurações ou variáveis de ambiente</param>
        public static void AddLoggerConfiguration(this IServiceCollection services, IConfiguration configuration, bool useConfigurationFile = true)
        {
            services.AddLogging(builder =>
            {
                builder.ClearProviders();

                builder.AddConfiguration(configuration.GetSection("Logging"));

                #region NetStash

                var useNetStash = false;

                if (useConfigurationFile)
                {
                    var netStashSection = configuration.GetSection("NetStash");
                    if (netStashSection != null)
                    {
                        var netStashHost = netStashSection.GetValue<string>("Host");

                        if (!string.IsNullOrEmpty(netStashHost))
                        {
                            builder.Services.Configure<NetStashOptions>(netStashSection);
                            useNetStash = true;
                        }
                    }
                }
                else
                {
                    var netStashServer = configuration.GetValue<string>("NETSTASH_HOST");
                    var netStashPort = configuration.GetValue<string>("NETSTASH_PORT");
                    var netStashAppName = configuration.GetValue<string>("NETSTASH_APPNAME");

                    if (!string.IsNullOrEmpty(netStashServer) &&
                        !string.IsNullOrEmpty(netStashPort) &&
                        !string.IsNullOrEmpty(netStashAppName))
                    {
                        builder.Services.Configure<NetStashOptions>(options =>
                        {
                            options.Host = netStashServer;
                            options.Port = Int32.Parse(netStashPort);
                            options.AppName = netStashAppName;
                        });

                        useNetStash = true;
                    }
                }

                if (useNetStash)
                {
                    builder.Services.PostConfigure<NetStashOptions>(options =>
                    {
                        var extraValues = new Dictionary<string, string>
                        {
                            {"Environment", configuration.GetValue<string>("ASPNETCORE_ENVIRONMENT") ?? "Production"},
                            {"Version", Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}
                        };

                        options.ExtraValues = extraValues;
                    });

                    builder.AddNetStash();
                }

                #endregion

                #region Sentry

                var sentryDsn = useConfigurationFile ?
                                configuration.GetSection("Sentry")?.GetValue<string>("Dsn") :
                                configuration.GetValue<string>("SENTRY_DSN");

                if (!string.IsNullOrEmpty(sentryDsn))
                {
                    builder.AddSentry(sentryDsn);
                }

                #endregion

                if (configuration.IsDevelopment())
                    builder.AddDebug();
            });
        }
    }
}