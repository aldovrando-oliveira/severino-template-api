using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Severino.Template.Api.Business;
using Severino.Template.Api.Controllers.ViewModels.Users;
using Severino.Template.Api.Infra.Configurations;
using Severino.Template.Api.Models;
using Severino.Template.Api.Repositories;

namespace Severino.Template.Api
{
    /// <summary>
    /// Extensão da interface <see cref="IServiceCollection"/> para adicionar configuração ao container de injeção de dependência
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        private static bool _mapperInitialized = false;
        private static object _mapperLock = new object();
        
        /// <summary>
        /// Adiciona as configurações de repositórios
        /// </summary>
        /// <param name="services">Coleção com os objetos que serão gerenciados pelo container de injeção de dependência</param>
        /// <param name="configuration">Objeto com as configurações de ambiente e da aplicação</param>
        public static void AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            var configurationFileUse = true; // for use environment variables turn this to false
            var databaseName = "Template"; // database configuration name reference

            services.AddDbContext<TemplateContext>(options => options.UseMySql(configuration.GetMySqlConnectionString(databaseName, configurationFileUse)));

            // for more configurations info: https://github.com/arch/UnitOfWork
            services.AddUnitOfWork<TemplateContext>();
        }

        /// <summary>
        /// Adiciona as configurações para entidades de negócio 
        /// </summary>
        /// <param name="services">Coleção com os objetos que serão gerenciados pelo container de injeção de dependência</param>
        public static void AddBusiness(this IServiceCollection services)
        {
            services.AddTransient<UserBusiness>();
        }

        /// <summary>
        /// Adiciona a configuração de mapeamento entre as entidades e VMs da aplicação
        /// </summary>
        /// <param name="services">Coleção com os objetos que serão gerenciados pelo container de injeção de dependência</param>
        public static void AddMapper(this IServiceCollection services)
        {
            lock (_mapperLock)
            {
                if (_mapperInitialized) 
                    return;
                
                Mapper.Initialize(cfg =>
                {
                    #region ViewModel To Model

                    cfg.CreateMap<CreateUserViewModelRequest, User>();
                    cfg.CreateMap<UpdateUserViewModelRequest, User>();

                    #endregion

                    #region Model To ViewModel

                    cfg.CreateMap<User, CreateUserViewModelResponse>();
                    cfg.CreateMap<User, GetUserByIdViewModelResponse>();
                    cfg.CreateMap<User, UpdateUserViewModelResponse>();

                    #endregion
                });
                _mapperInitialized = true;
            }
        }
    }
}