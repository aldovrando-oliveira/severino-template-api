using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Severino.Template.Api
{
    /// <summary>
    /// Extensões para adicionar a documentação de APIs ao projeto
    /// </summary>
    public static class SwagggerExtensions
    {
        /// <summary>
        /// Adiciona a documentação da API ao container de injeção de dependência
        /// </summary>
        /// <param name="services">Coleção com os objetos que serão gerenciados pelo container de injeção de dependência</param>
        public static void AddAppDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "Api Template",
                        Version = "v1",
                        Description = "Boilerplate para criação de novas APIs",
                        Contact = new Contact
                        {
                            Name = "Desenvolvedor",
                            Url = "www.google.com",
                            Email = "contato@gmail.com"
                        }
                    });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                config.IncludeXmlComments(xmlPath);
                
                config.DescribeStringEnumsInCamelCase();
                config.DescribeAllEnumsAsStrings();
                config.DescribeAllParametersInCamelCase();
            });
        }

        /// <summary>
        /// Utiliza o middleware para expor a documentação das APIs
        /// </summary>
        /// <param name="app">Builder de configuração da aplicação</param>
        public static void UseAppDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Template API");
            });

        }
    }
}