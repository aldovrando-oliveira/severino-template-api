using App.Metrics;
using App.Metrics.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Severino.Template.Api
{
    /// <summary>
    /// Configuração para coleta e envio das métricas da aplicação
    /// </summary>
    public static class MetricsExtensions
    {
        /// <summary>
        /// Adiciona a configuração das métricas ao container de injeção de dependência
        /// </summary>
        /// <param name="services">Container de configuração da injetção de dependência</param>
        /// <param name="configuration">Objeto com as configurações da aplicação</param>
        public static void AddAppMetrics(this IServiceCollection services, IConfiguration configuration)
        {
            var metrics = AppMetrics.CreateDefaultBuilder()
                .OutputMetrics.AsPrometheusProtobuf()
                .OutputMetrics.AsPrometheusPlainText()
                .Configuration.ReadFrom(configuration)
                .Build();

            services.AddMetrics(metrics)
                .AddMetricsTrackingMiddleware(configuration)
                .AddMetricsEndpoints(configuration);
        }

        /// <summary>
        /// Adiciona os middlewares de métricas na aplicação
        /// </summary>
        /// <param name="app">Builder de configuração da aplicação</param>
        public static void UseAppMetrics(this IApplicationBuilder app)
        {
            app.UseMetricsAllMiddleware();
        }
    }
}