using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Severino.Template.Api.Infra.Configurations;

namespace Severino.Template.Api.Repositories.Configuration.Context
{
    /// <inheritdoc />
    /// <summary>
    /// Classe responsável por criar uma instância de DbContext para criar um novo migration ou
    /// executar atualização manual do banco de dados
    /// </summary>
    /// <typeparam name="T">Tipo do DbContext que deve ser utilizado</typeparam>
    public abstract class DesignTimeMySqlDbContextFactory<T> : IDesignTimeDbContextFactory<T>
        where T : DbContext
    {
        private readonly string _schemaName;
        private readonly bool _configFile;

        /// <summary>
        /// Cria uma instância de <see cref="DesignTimeMySqlDbContextFactory{T}" />
        /// </summary>
        /// <param name="schemaName">Nome da configuração do banco de dados</param>
        /// <param name="configFile">Se "true" utiliza o arquivo de configuração, senão as variaveis de ambiente</param>
        protected DesignTimeMySqlDbContextFactory(string schemaName, bool configFile)
        {
            _schemaName = schemaName ?? throw new ArgumentNullException(nameof(schemaName));
            _configFile = configFile;
        }

        /// <inheritdoc />
        /// <param name="schemaName">Nome da configuração do banco de dados</param>
        protected DesignTimeMySqlDbContextFactory(string schemaName) : this(schemaName, false) { }

        /// <inheritdoc />
        public T CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<T>();
            var connectionString = BuildConnectionString(_schemaName, _configFile);
            builder.UseMySql(connectionString);
            var dbContext = (T)Activator.CreateInstance(
                typeof(T),
                builder.Options);
            return dbContext;
        }

        /// <summary>
        /// Retorna a string de conexão com o banco de dados
        /// </summary>
        /// <param name="schemaName">Nome da configução com o banco de dados</param>
        /// <param name="configFile">Se "true" utiliza o arquivo de configuração, senão as variaveis de ambiente</param>
        /// <returns>String de conexão com o banco de dados</returns>
        protected virtual string BuildConnectionString(string schemaName, bool configFile = false)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").ToLower()}.json", optional: true)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            return configuration.GetMySqlConnectionString(schemaName, configFile);
        }
    }
}