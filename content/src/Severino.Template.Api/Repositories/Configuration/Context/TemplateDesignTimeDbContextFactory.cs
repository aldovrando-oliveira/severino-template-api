namespace Severino.Template.Api.Repositories.Configuration.Context
{
    /// <inheritdoc />
    public class TemplateDesignTimeDbContextFactory : DesignTimeMySqlDbContextFactory<TemplateContext>
    {
        /// <inheritdoc />
        public TemplateDesignTimeDbContextFactory() : base("Template", true)
        {
        }
    }
}