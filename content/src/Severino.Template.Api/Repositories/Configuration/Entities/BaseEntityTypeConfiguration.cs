using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Severino.Template.Api.Models;

namespace Severino.Template.Api.Repositories.Configuration.Entities
{
    /// <summary>
    /// Configuração básica das entidades que fazem parte do modelo de dados
    /// </summary>
    /// <typeparam name="TEntity">Tipo da entidade que será configurada</typeparam>
    public abstract class BaseEntityTypeConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : class, IModel
    {
        /// <inheritdoc />
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            SetPrimaryKey(builder);

            builder.Property(x => x.Id).HasColumnName("id").IsRequired();
            builder.Property(x => x.CreatedAt).HasColumnName("created_at").IsRequired();
            builder.Property(x => x.UpdatedAt).HasColumnName("updated_at").IsRequired();
        }

        /// <summary>
        /// Define qual será a chave primária da entidade
        /// </summary>
        /// <param name="builder">Builder para configuração da entidade</param>
        protected virtual void SetPrimaryKey(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}