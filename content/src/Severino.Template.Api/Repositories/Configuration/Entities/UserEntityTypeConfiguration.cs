using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Severino.Template.Api.Models;

namespace Severino.Template.Api.Repositories.Configuration.Entities
{
    /// <summary>
    /// Configuração da entidade <see cref="User"/> com o banco de dados
    /// </summary>
    public class UserEntityTypeConfiguration : BaseEntityTypeConfiguration<User>
    {
        /// <summary>
        /// Realiza o mapeamento da entidade <see cref="User"/> com a tabela e campos do banco de dados
        /// </summary>
        /// <param name="builder">Builder para configuração do contexto</param>
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);

            builder.ToTable("user");
            builder.Property(x => x.Name).HasColumnName("name").HasMaxLength(50).IsRequired();
            builder.Property(x => x.Email).HasColumnName("email").HasMaxLength(150).IsRequired();
        }
    }
}