using Microsoft.EntityFrameworkCore;
using Severino.Template.Api.Models;
using Severino.Template.Api.Repositories.Configuration.Entities;

namespace Severino.Template.Api.Repositories
{
    /// <summary>
    /// Contexto de acesso ao banco de dados
    /// </summary>
    public class TemplateContext : DbContext
    {
        /// <summary>
        /// Retorna uma nova instância de <see cref="TemplateContext"/>
        /// </summary>
        public TemplateContext() { }

        /// <summary>
        /// Retorna uma nova instância de <see cref="TemplateContext"/>
        /// </summary>
        /// <param name="options">Dados de configuração do contexto</param>
        public TemplateContext(DbContextOptions options) : base(options) { }
        
        /// <summary>
        /// Dados de usuários
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
        }
    }
}