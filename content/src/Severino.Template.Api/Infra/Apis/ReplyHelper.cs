using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Severino.Template.Api.Infra.Exceptions;

namespace Severino.Template.Api.Infra.Apis
{
    /// <summary>
    /// Classe auxiliar para formatar as respostas das controllers
    /// </summary>
    public class ReplyHelper
    {
        /// <summary>
        /// Formata a exceção não tratadas que chegarem nas controllers
        /// </summary>
        /// <param name="ex">Exceção que será tratada para usar no retorno da API</param>
        /// <returns></returns>
        public static IActionResult Error(Exception ex)
        {
            switch (ex)
            {
                case EntityNotFoundException entityNotFoundException:
                    return new NotFoundObjectResult(new ErrorViewModel
                    {
                        Status = (int)HttpStatusCode.NotFound,
                        Message = entityNotFoundException.Message,
                        Details = entityNotFoundException.StackTrace
                    });

                case ConflictException conflictException:
                    return new ConflictObjectResult(new ErrorViewModel
                    {
                        Status = (int)HttpStatusCode.Conflict,
                        Message = conflictException.Message
                    });

                default:
                    return new ObjectResult(new ErrorViewModel
                    {
                        Status = (int)HttpStatusCode.InternalServerError,
                        Message = $"Unexpected error processing request. ({ex.Message})",
                        Details = ex.StackTrace
                    })
                    {
                        StatusCode = 500
                    };
            }
        }
    }
}