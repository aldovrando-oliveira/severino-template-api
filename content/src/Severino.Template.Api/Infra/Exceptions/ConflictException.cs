using System;

namespace Severino.Template.Api.Infra.Exceptions
{
    /// <summary>
    /// Exceção de conflitos nas entidades
    /// </summary>
    public class ConflictException : Exception
    {
        /// <summary>
        /// Cria uma nova instância de <see cref="ConflictException"/>
        /// </summary>
        /// <param name="message">Mensagem descritiva da exceção</param>
        public ConflictException(string message) : base(message)
        {
        }
    }
}