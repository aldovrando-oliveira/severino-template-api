using System;

namespace Severino.Template.Api.Infra.Exceptions
{
    /// <summary>
    /// Exceção de configuração não encontrada
    /// </summary>
    public class ConfigurationNotFoundException : Exception
    {
        /// <summary>
        /// Chave de configuração
        /// </summary>
        public string ConfigurationKey { get; }
        
        /// <summary>
        /// Cria uma nova instância de <see cref="ConfigurationNotFoundException"/>
        /// </summary>
        /// <param name="configurationKey">Chave de configuração</param>
        public ConfigurationNotFoundException(string configurationKey) : base($"Configuration key '{configurationKey}' not found") 
        {
            ConfigurationKey = configurationKey;
        }
    }
}