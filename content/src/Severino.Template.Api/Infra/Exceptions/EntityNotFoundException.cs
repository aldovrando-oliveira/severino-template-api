using System;

namespace Severino.Template.Api.Infra.Exceptions
{
    /// <summary>
    /// Exceção de entidade não encontrada
    /// </summary>
    public class EntityNotFoundException : Exception
    {
        /// <summary>
        /// Nome da entidade
        /// </summary>
        private string Entity { get; }
        
        /// <summary>
        /// Cria uma nova instância de <see cref="EntityNotFoundException"/>
        /// </summary>
        /// <param name="entityName">Nome da entidade</param>
        public EntityNotFoundException(string entityName) : base($"Entity '{entityName}' not found") 
        {
            Entity = entityName;
        }
    }
}