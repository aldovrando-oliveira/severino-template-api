using System.Linq;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Severino.Template.Api.Infra.Exceptions;

namespace Severino.Template.Api.Infra.Configurations
{
    /// <summary>
    /// Extensões para ajuda na construção das configurações de conexões com os bancos de dados
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Retorna a configuração da conexão com o banco MySql
        /// </summary>
        /// <param name="configuration">Configuração do ambiente</param>
        /// <param name="schemaName">Nome do banco de dados. Igual o nome utilizado nas configurações</param>
        /// <param name="configFile">Indica que deve ser utilizado o arquivo de configurações enão as variáveis de ambiente</param>
        /// <returns>Texto com a configuração da conexão com o banco MySql</returns>
        /// <exception cref="ConfigurationNotFoundException">Quando está sendo utilizado o arquivo de configurção e os dados do banco não foram encontrados</exception>
        public static string GetMySqlConnectionString(this IConfiguration configuration, string schemaName, bool configFile = false)
        {
            if (configFile)
                if (!configuration.HasSection("Database"))
                    throw new ConfigurationNotFoundException("Database");
                else if (!configuration.GetSection("Database").HasSection(schemaName))
                    throw new ConfigurationNotFoundException($"Database:{schemaName}");

            var conStrBuilder = new MySqlConnectionStringBuilder
            {
                Server = configuration.GetValue<string>(configFile
                    ? $"Database:{schemaName}:Server"
                    : $"{schemaName.ToUpper()}_DB_SERVER"),
                Database = configuration.GetValue<string>(configFile
                    ? $"Database:{schemaName}:Schema"
                    : $"{schemaName.ToUpper()}_DB_SCHEMA"),
                Port = configuration.GetValue<uint>(configFile
                    ? $"Database:{schemaName}:Port"
                    : $"{schemaName.ToUpper()}_DB_PORT"),
                UserID = configuration.GetValue<string>(configFile
                    ? $"Database:{schemaName}:User"
                    : $"{schemaName.ToUpper()}_DB_USER"),
                Password = configuration.GetValue<string>(configFile
                    ? $"Database:{schemaName}:Password"
                    : $"{schemaName.ToUpper()}_DB_PASSWORD")
            };

            return conStrBuilder.ToString();
        }

        /// <summary>
        /// Verifica se a sessão existe
        /// </summary>
        /// <param name="configuration">Configuração do ambiente</param>
        /// <param name="section">Nome da seção</param>
        /// <returns>Retorna true caso a seão exista</returns>
        public static bool HasSection(this IConfiguration configuration, string section)
        {
            return configuration.GetChildren().Any(x => x.Key == section);
        }
        
        /// <summary>
        /// Verifica se a sessão existe 
        /// </summary>
        /// <param name="configurationSection">Subsessão na configuração do ambiente</param>
        /// <param name="section">Nome da sessão</param>
        /// <returns>Retorna true caso a seão exista</returns>
        public static bool HasSection(this IConfigurationSection configurationSection, string section)
        {
            return configurationSection.GetChildren().Any(x => x.Key == section);
        }

        /// <summary>
        /// Verifica se o ambiente esta parametrizado para desenvolvimento
        /// </summary>
        /// <param name="configuration">Objeto com as configurações do ambiente</param>
        /// <returns>Se o ambiente estiver parametrizado para desenvolvimento retorna 'true'</returns>
        public static bool IsDevelopment(this IConfiguration configuration)
        {
            var environment = configuration.GetValue<string>("ASPNETCORE_ENVIRONMENT") ?? "Production";

            return environment.ToLower() == "development";
        }

        /// <summary>
        /// Verifica se o ambiente esta parametrizado para homologação
        /// </summary>
        /// <param name="configuration">Objeto com as configurações do ambiente</param>
        /// <returns>Se o ambiente estiver parametrizado para homologação retorna 'true'</returns>
        public static bool IsStaging(this IConfiguration configuration)
        {
            var environment = configuration.GetValue<string>("ASPNETCORE_ENVIRONMENT") ?? "Production";

            return environment.ToLower() == "staging";
        }

        /// <summary>
        /// Verifica se o ambiente esta parametrizado para produção
        /// </summary>
        /// <param name="configuration">Objeto com as configurações do ambiente</param>
        /// <returns>Se o ambiente estiver parametrizado para produção retorna 'true'</returns>
        public static bool IsProduction(this IConfiguration configuration)
        {
            var environment = configuration.GetValue<string>("ASPNETCORE_ENVIRONMENT") ?? "Production";

            return environment.ToLower() == "production";
        }

    }
}