using System;
using System.ComponentModel.DataAnnotations;

namespace Severino.Template.Api.Controllers.ViewModels.Users
{
    /// <summary>
    /// Objeto com as informações do usuário encontrado
    /// </summary>
    public class GetUserByIdViewModelResponse
    {
        /// <summary>
        /// Código de identificação do usuário
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Email de login do usuário
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Data de criação do usuário
        /// </summary>
        public DateTimeOffset CreatedAt { get; set; }
        
        /// <summary>
        /// Data da ultima atualização do usuário
        /// </summary>
        public DateTimeOffset UpdatedAt { get; set; }
    }
}