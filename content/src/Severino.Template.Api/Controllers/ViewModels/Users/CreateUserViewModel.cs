using System;
using System.ComponentModel.DataAnnotations;

namespace Severino.Template.Api.Controllers.ViewModels.Users
{
    /// <summary>
    /// Objeto com as informações para a criação de um novo usuário
    /// </summary>
    public class CreateUserViewModelRequest
    {
        /// <summary>
        /// Nome do usuário
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Email de login do usuário
        /// </summary>
        [Required]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }
    }

    /// <summary>
    /// Objeto com os dados do usuário criado
    /// </summary>
    public class CreateUserViewModelResponse
    {
        /// <summary>
        /// Código de identificação do usuário
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Email de login do usuário
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Data de criação do usuário
        /// </summary>
        public DateTimeOffset CreatedAt { get; set; }
        
        /// <summary>
        /// Data da ultima atualização do usuário
        /// </summary>
        public DateTimeOffset UpdatedAt { get; set; }
    }
}