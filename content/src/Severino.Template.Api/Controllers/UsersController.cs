using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Severino.Template.Api.Business;
using Severino.Template.Api.Controllers.ViewModels.Users;
using Severino.Template.Api.Infra.Apis;
using Severino.Template.Api.Models;

namespace Severino.Template.Api.Controllers
{
    /// <summary>
    /// Controller que disponibiliza os recursos para manipulação de usuários
    /// </summary>
    [Authorize]
    [Route("api/users")]
    [ApiController]
    [Produces("application/json")]
    public class UsersController : ControllerBase
    {
        private readonly UserBusiness _userBusiness;

        /// <summary>
        /// Cria uma nova instância da classe <see cref="UsersController"/>
        /// </summary>
        /// <param name="userBusiness">Instância do tipo <see cref="UserBusiness"/></param>
        public UsersController(UserBusiness userBusiness)
        {
            _userBusiness = userBusiness ?? throw new ArgumentNullException(nameof(userBusiness));
        }

        /// <summary>
        /// Cria um novo usuário
        /// </summary>
        /// <param name="request">Informações para a criação do novo usuário</param>
        /// <response code="201">Usuário criado com sucesso</response>
        /// <response code="500">Erro inesperado</response>
        [HttpPost(Name = "CreateUser")]
        [ProducesResponseType(201, Type = typeof(CreateUserViewModelResponse))]
        public async Task<IActionResult> CreateAsync([FromBody]CreateUserViewModelRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var newUser = Mapper.Map<User>(request);

            try
            {
                var response = Mapper.Map<CreateUserViewModelResponse>(await _userBusiness.CreateAsync(newUser));
                return CreatedAtRoute("GetUserById", new {id = response.Id}, response);
            }
            catch (Exception ex)
            {
                return ReplyHelper.Error(ex);
            }
        }

        /// <summary>
        /// Consulta usuários pelo código identificador
        /// </summary>
        /// <param name="id">Código para realizar a consulta</param>
        /// <response code="200">Consulta realizada com sucesso</response>
        /// <response code="404">Usuário não encontrado</response>
        /// <response code="500">Erro inesperado</response>
        [HttpGet("{id}", Name = "GetUserById")]
        [ProducesResponseType(200, Type = typeof(GetUserByIdViewModelResponse))]
        public async Task<IActionResult> GetByIdAsync([FromRoute]Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var response = Mapper.Map<GetUserByIdViewModelResponse>(await _userBusiness.GetByIdAsync(id));
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ReplyHelper.Error(ex);
            }
        }

        /// <summary>
        /// Atualiza os dados de um usuário
        /// </summary>
        /// <param name="id">Código do usuário a ser atualizado</param>
        /// <param name="request">Informações que serão atualizadas</param>
        /// <response code="200">Atualização realizada com sucesso</response>
        /// <response code="404">Usuário não encontrado</response>
        /// <response code="500">Erro inesperado</response>
        [HttpPut("{id}", Name = "UpdateUser")]
        [ProducesResponseType(200, Type = typeof(UpdateUserViewModelResponse))]
        public async Task<IActionResult> UpdateAsync([FromRoute]Guid id, [FromBody]UpdateUserViewModelRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var updatedUser = Mapper.Map<User>(request);
            try
            {
                var response = Mapper.Map<UpdateUserViewModelResponse>(await _userBusiness.UpdateAsync(id, updatedUser));
                return Ok(response);
            }
            catch (Exception ex)
            {
                return ReplyHelper.Error(ex);
            }
        }

        /// <summary>
        /// Remove um usuário
        /// </summary>
        /// <param name="id">Código do usuário que será removido</param>
        /// <response code="204">Exclusão realiza com sucesso</response>
        /// <response code="404">Usuário não encontrado</response>
        /// <response code="500">Erro inesperado</response>
        [HttpDelete("{id}", Name = "DeleteUser")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DeleteAsync([FromRoute]Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                await _userBusiness.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return ReplyHelper.Error(ex);
            }
        }
    }
}