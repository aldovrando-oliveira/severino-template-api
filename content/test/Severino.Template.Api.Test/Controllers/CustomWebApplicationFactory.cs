using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Severino.Template.Api.Repositories;
using Severino.Template.Api.Test.Seeders;

namespace Severino.Template.Api.Test.Controllers
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddOptions()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                services.AddDbContext<TemplateContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryApp");
                    options.UseInternalServiceProvider(serviceProvider);
                });

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scoppedServices = scope.ServiceProvider;
                    var db = scoppedServices.GetRequiredService<TemplateContext>();

                    var logger = scoppedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    db.Database.EnsureCreated();

                    try
                    {
                        UserSeedData.SeedData(db);
                    }
                    catch (Exception e)
                    {
                        logger.LogError(e, "Erro ao inserir dados de teste no banco de dados: {0}", e.Message);
                        Console.WriteLine(e);
                        throw;
                    }
                }
            });
        }
    }
}