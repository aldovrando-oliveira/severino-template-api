using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Severino.Template.Api.Controllers.ViewModels.Users;
using Severino.Template.Api.Test.Seeders;
using Xunit;

namespace Severino.Template.Api.Test.Controllers.User
{
    public class UserControllerGetByIdTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public UserControllerGetByIdTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
        }

        private async Task<HttpResponseMessage> GetUserById(string id)
        {
            var httpResponse = await _client.GetAsync($"/api/users/{id}");
            return httpResponse;
        }
        
        [Fact]
        public async Task GetByIdSuccessAsync()
        {
            var response = await GetUserById(UserSeedData.UserGetById.Id.ToString());
            response.EnsureSuccessStatusCode();
            
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var stringResponse = await response.Content.ReadAsStringAsync();
            var userFound = JsonConvert.DeserializeObject<GetUserByIdViewModelResponse>(stringResponse);
            
            Assert.Equal(UserSeedData.UserGetById.Id, userFound.Id);
            Assert.Equal(UserSeedData.UserGetById.Name, userFound.Name);
            Assert.Equal(UserSeedData.UserGetById.Email, userFound.Email);
            Assert.NotEqual(default(DateTimeOffset), userFound.CreatedAt);
            Assert.NotEqual(default(DateTimeOffset), userFound.UpdatedAt);
        }

        [Fact]
        public async Task GetByIdNotFoundAsync()
        {
            var id = Guid.NewGuid().ToString();
            var response = await GetUserById(id);
           
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData("2a574e06-207f-4905-9bfe-6870af6e46c")]
        [InlineData("2a574e06-207f")]
        [InlineData("abcd")]
        [InlineData("1")]
        public async Task GetByIdInvalidAsync(string id)
        {
            var response = await GetUserById(id);
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}