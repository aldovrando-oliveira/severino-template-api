using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Severino.Template.Api.Controllers.ViewModels.Users;
using Severino.Template.Api.Test.Seeders;
using Xunit;

namespace Severino.Template.Api.Test.Controllers.User
{
    public class UserControllerUpdateTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        
        public UserControllerUpdateTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
        }


        [Fact]
        public async Task UpdateUserNotFoundAsync()
        {
            var request = new UpdateUserViewModelRequest{ Name = "Update Teste", Email = "update.teste@gmail.com"};
            var response = await _client.PutAsJsonAsync($"/api/users/{Guid.NewGuid().ToString()}", request);
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public async Task UpdateUserNameInvalidAsync(string name)
        {
            var request = new UpdateUserViewModelRequest{ Name = name, Email = "update.teste@gmail.com"};
            var response = await _client.PutAsJsonAsync($"/api/users/{Guid.NewGuid().ToString()}", request);
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData("joao")]
        [InlineData("joao@")]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("joao.silva@")]
        public async Task UpdateUserEmailInvalidAsync(string email)
        {
            var request = new UpdateUserViewModelRequest{ Name = "Nome Atualizado", Email = email};
            var response = await _client.PutAsJsonAsync($"/api/users/{Guid.NewGuid().ToString()}", request);
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData("2a574e06-207f-4905-9bfe-6870af6e46c")]
        [InlineData("2a574e06-207f")]
        [InlineData("abcd")]
        [InlineData("1")]
        public async Task UpdateUserIdInvalidAsync(string id)
        {
            var request = new UpdateUserViewModelRequest { Name = "Zé da Silva", Email = "ze.da.silva@gmail.com"};
            var response = await _client.PutAsJsonAsync($"/api/users/{id}", request);
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateUserSuccessAsync()
        {
            var request = new UpdateUserViewModelRequest{ Name = "Nome Atualizado", Email = "email.atualizado@bol.com"};
            var response = await _client.PutAsJsonAsync($"/api/users/{UserSeedData.UpdateUser.Id.ToString()}", request);
            
            Assert.True(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var userUpdate = JsonConvert.DeserializeObject<UpdateUserViewModelResponse>(await response.Content.ReadAsStringAsync());
            
            Assert.Equal(UserSeedData.UpdateUser.Id, userUpdate.Id);
            Assert.Equal("Nome Atualizado", userUpdate.Name);
            Assert.Equal("email.atualizado@bol.com", userUpdate.Email);
            Assert.NotEqual(default(DateTimeOffset), userUpdate.CreatedAt);
            Assert.NotEqual(default(DateTimeOffset), userUpdate.UpdatedAt);
        }
    }
}