using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Severino.Template.Api.Controllers.ViewModels.Users;
using Xunit;

namespace Severino.Template.Api.Test.Controllers.User
{
    public class UserControllerCreateTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public UserControllerCreateTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
        }

        private async Task<HttpResponseMessage> PostCreateUser(CreateUserViewModelRequest request)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, MediaTypeNames.Application.Json);
            var httpResponse = await _client.PostAsync("/api/users", stringContent);

            return httpResponse;
        }

        [Theory]
        [InlineData("Maria", "maria@gmail.com")]
        [InlineData("Zé da Captinga", "ze.da.capatiginga@gmail.com")]
        [InlineData("Canarinho da Silva", "canarinho.silva@brasil.com.br")]
        public async Task CreateUserSuccessAsync(string name, string email)
        {
            var newUser = new CreateUserViewModelRequest
            {
                Name = name,
                Email = email
            };

            var response = await PostCreateUser(newUser);
            response.EnsureSuccessStatusCode();

            var stringResponse = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<CreateUserViewModelResponse>(stringResponse);
            
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.NotEqual(Guid.Empty, user.Id);
            Assert.Equal(name, user.Name);
            Assert.Equal(email, user.Email);
            Assert.NotEqual(default(DateTimeOffset), user.CreatedAt);
            Assert.NotEqual(default(DateTimeOffset), user.UpdatedAt);
        }

        [Theory]
        [InlineData("joao")]
        [InlineData("joao@")]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("joao.silva@")]
        public async Task CreateUserInvalidEmailAsync(string email)
        {
            var newUser = new CreateUserViewModelRequest
            {
                Name = "Teste",
                Email = email
            };

            var response = await PostCreateUser(newUser);
            
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData(" ")]
        public async Task CreateUserNameInvalidAsync(string name)
        {
            var newUser = new CreateUserViewModelRequest
            {
                Name = name,
                Email = "teste@gmail.com"
            };

            var response = await PostCreateUser(newUser);
            
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}