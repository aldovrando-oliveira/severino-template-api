using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using Severino.Template.Api.Test.Seeders;
using Xunit;

namespace Severino.Template.Api.Test.Controllers.User
{
    public class UserControllerDeleteTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        
        public UserControllerDeleteTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
        }

        [Fact]
        public async Task DeleteUserNotFoundAsync()
        {
            var response = await _client.DeleteAsync($"/api/users/{Guid.NewGuid().ToString()}");
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        
        [Theory]
        [InlineData("2a574e06-207f-4905-9bfe-6870af6e46c")]
        [InlineData("2a574e06-207f")]
        [InlineData("abcd")]
        [InlineData("1")]
        public async Task DeleteUserIdInvalidAsync(string id)
        {
            var response = await _client.DeleteAsync($"/api/users/{id}");
            
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUserSuccessAsync()
        {
            var response = await _client.DeleteAsync($"/api/users/{UserSeedData.DeleteUser.Id.ToString()}");
            
            Assert.True(response.IsSuccessStatusCode);
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
    }
}