using Severino.Template.Api.Models;
using Severino.Template.Api.Repositories;

namespace Severino.Template.Api.Test.Seeders
{
    public static class UserSeedData
    {
        public static readonly User UserGetById = new User
        {
            Name = "Usuário 1",
            Email = "usuario@gmail.com"
        };
        
        public static readonly User UpdateUser = new User
        {
            Name = "Teste Update",
            Email = "teste.update@gmail.com"
        };

        public static readonly User DeleteUser = new User
        {
            Name = "Teste Delete",
            Email = "teste.delete@gmail.com"
        };
        
        public static void SeedData(TemplateContext context)
        {
            context.Users.Add(UserGetById);
            context.Users.Add(UpdateUser);
            context.Users.Add(DeleteUser);

            context.SaveChanges();
        }
    }
}