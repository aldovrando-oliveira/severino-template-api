# PROJECT-TITLE

PROJECT-DESCRIPTION

## Recursos
 - Conexão com o banco MySql
 - Documentação dos Endpoints com Swagger
 - Logger via TCP/IP.
 - Documentação APIs
 - Monitoramento com Prometheus e Grafana
 - Integrações: Sentry
 - Exemplo de objetos dos tipos Controller, Business, Repository e Model

## Configurações
Instruções para configuração da aplicação

### **Banco de Dados**
As configurações para acesso ao banco de dados podem ser realizadas por variáveis de ambiente ou pelo arquivo de configurações `appsettings.json`.

| Variável              | Descrição                                                 | Tipo      | Obrigatório 
| ----------------------| --------------------------------------------------------- | --------- |-----
|{Database}_DB_SERVER   | Servidor de Banco de dados                                | Texto     | Sim
|{Database}_DB_SCHEMA   | Nome do banco de dados                                    | Texto     | Sim
|{Database}_DB_PORT     | Porta para conexão com o banco de dados                   | Texto     | Sim
|{Database}_DB_USER     | Usuário para conexão com o banco de dados                 | Texto     | Sim
|{Database}_DB_PASSWORD | Senha de autenticação para conexão com o banco de dados   | Texto     | Sim

Para configuração via arquivo `appsettings.json` deve ser utiliziado o seguinte padrão
```json
{
  "Database": {
    "{Database}": {
      "Server": "localhost",
      "Schema": "template",
      "Port": "3306",
      "User": "template",
      "Password": "template"
    }
  }
}
```
Em ambos modelos de configuração o valor de `{Database}` deve ser substituído pelo nome de referência utilizado para carregar as configurações. Para definir o nome de referência de conexão com o banco é preciso atualizar a configuração do arquivo `App_Start/ServiceCollectionExtensions.cs` na linha demonstrada abaixo:
```csharp
public static void AddRepositories(this IServiceCollection services, IConfiguration configuration)
{
    var configurationFileUse = true; // for use environment variables turn this to false
    var databaseName = "Template"; // database configuration name reference

    services.AddDbContextPool<TemplateContext>(options => options.UseMySql(configuration.GetMySqlConnectionString(databaseName, configurationFileUse)));
}
```

### **Loggers**
Foi utilizada a convenção de implemantação padrão de Log utilizando os recursos disponibilizados pela biblioteca `Microsoft.Extensions.Logging`. Essa implementação permite que novos mecanismos de log sejam adicionados sem a necessidade de adequação de codificação.

Exexmplo de utilização:
```csharp
public class ValuesController : Controller
{
  private ILogger<ValuesController> _logger;

  public ValuesController(ILogger<ValuesController> logger)
  {
    _logger = logger;
  }

  [HttpGet("")]
  public IActionResult Get()
  {
    _logger.LogDebug("Montado objeto de retorno da consulta");
    var values = new [] {"valor 1", "valor 2"};

    return Ok(values);
  }
}
```

Atualmente a aplicação da suporte para os mecanismos TCP/IP e Sentry.
> O mecanismo para **Console** é sempre ativo e o **Debug** é adicionado quando a variável de ambiente `ASPNETCORE_ENVIRONMENT` está devida como `Development`.

**TCP/IP**  
Várias ferramentas oferecem suporte ao transporte das mensagens de log diretamente pela conexão TCP/IP, como por exemplo o [Logstash](https://www.elastic.co/products/logstash).
Para a implementação desse mecanismo foi utilizada a biblioteca [NetStash.Extensions.Logging](https://github.com/aldovrando-oliveira/NetStash.Extensions.Logging).  

A configuração pode ser realizada por variáveis de ambiente e pelo arquivo de configuração `appsettings.json` como exemplificado abaixo:  
 
| Variável                  | Descrição                                     | Tipo      | Obrigatório 
| ------------------------- | --------------------------------------------- | --------- |-----
| NETSTASH_HOST             | Servidor de aplicação que receberá os logs    | Texto     | Sim
| NETSTASH_PORT             | Porta para conexão com o servidor             | Texto     | Sim
| NETSTASH_APPNAME          | Nome do App que está registrando o log.       | Texto     | Sim

> Caso a variável de ambiente `NETSTASH_HOST` não esteja configurada o appender para log TCP não será utilizado.  
                              
Para realizar a configuração pelo arquivo `appsettings.json` deve ser utilizado o padrão abaixo
```json
{
  "Logging": {
    "NetStash": {
      "LogLevel": {
        "Default": "Debug",
        "System": "Warning",
        "Microsoft": "Warning"
      }
    }
  },
  "NetStash": {
    "AppName": "{AppName}",
    "Host": "{Server}",
    "Port": "{Port}"
  }
}
```
É possível incluir informações adicionais para serem anexadas as mensagens de log incluindo o objeto `ExtraValues` dentro do objeto `NetStash` que está no arquivo de configurações.

```json
{
  { ... },
  "NetStash": {
    "AppName": "{AppName}",
    "Host": "{Server}",
    "Port": "{Port}",
    "ExtraValues": {
        "Author: "João da Silva
    }
  }
}
```
Ou incluir informações adicionais dinamicamente pelo arquivo `App_Start/LoggerExtensions.cs`.
```csharp
public static void AddLoggerConfiguration(this IServiceCollection services, IConfiguration configuration)
{
    builder.Services.PostConfigure<NetStashOptions>(options =>
    {
        options.ExtraValues.Add("Environment",
            configuration.GetValue<string>("ASPNETCORE_ENVIRONMENT") ?? "Production");
        options.ExtraValues.Add("Version",
            Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>()
                .InformationalVersion);
    });
}
```

**Sentry**  

[Sentry](https://sentry.io/welcome/) é uma ferramenta para rastreio e notificação de erros. 
Para integração com a ferramenta foi utilizada a biblioteca [Sentry.Extensions.Logging](https://github.com/getsentry/sentry-dotnet) e sua configuração pode ser feita por variáveis de ambiente ou pelo arquivo de configuração

| Variável   | Descrição                                      | Tipo      | Obrigatório 
| ---------- | ---------------------------------------------- | --------- |-----
| SENTRY_DSN | URL do sentry para notificação de erros        | Texto     | Não

> Caso a variável de ambiente `SENTRY_DSN` não esteja configurada o appender para o Sentry não será utilizado.  
 
Para realizar a configuração pelo arquivo `appsettings.json` deve ser utilizado o padrão abaixo
```json
{
  "Sentry": {
      "Dsn": "{dsn}"
  }
}
```

Para definir se a configuração deve ser feita por váriaveis de ambiente ou pelo arquivo de configuração `appsettings.json` basta alterar o parametro do método `AddLoggerConfiguration` dentro da classe `Startup.cs`.
```csharp
public void ConfigureServices(IServiceCollection services)
{
    var useFileConfiguration = true;
    services.AddLoggerConfiguration(Configuration, useFileConfiguration);
}
```
## Documentação APIs
Para documentação das APIs foi utilizado o [Swagger](https://swagger.io/). Com ele é possível verificar os contratos das APIs e testar os endpoints.
Ele está acessível pelo endping `/swagger`.

![swagger](docs/images/swagger.png)

## Monitoramento
Para extração dos dados de monitoramento foi utilizada a biblioteca [AppMetrics](https://www.app-metrics.io/), o banco [Prometheus](https://prometheus.io/) e [Grafana](https://grafana.com/) para exibição.

![grafana prometheus dashboard](docs/images/grafana_prometheus.png)

> O Dashboard está disponível no [GrafanaLabs](https://grafana.com/dashboards/2204).

A configuração do monitoramento pode ser feito a partir do arquivo `appsettings.json` com o bloco abaixo:
```json
{
  "MetricsOptions": {
    "DefaultContextLabel": "Severino.Template.Api",
    "Enabled": true,
    "ReportingEnabled": true
  },
  "MetricsWebTrackingOptions": {
    "ApdexTrackingEnabled": true,
    "ApdexTSeconds": 0.8,
    "IgnoredHttpStatusCodes": [],
    "IgnoredRoutesRegexPatterns": [],
    "OAuth2TrackingEnabled": true
  },
  "MetricEndpointsOptions": {
    "MetricsEndpointEnabled": true,
    "MetricsTextEndpointEnabled": true,
    "EnvironmentInfoEndpointEnabled": true
  }
}
```
Os endpoints que devem ser utilizados pelo Prometheus para extração de dados são `/metrics` para Protobuf ou `/metrics-text` para texto comum.

Além desses endpoints é possível verificar a configuração do ambiente da aplicação pelo endpoing `/env`.

## Desenvolvimento
Para auxiliar no desenvolvimento do projeto é possível utilizar containers Docker, no arquivo `/src/docker-compose.yml` há configuração para utilização dos container de MySql, Prometheus e Grafana.
As configurações básicas para o Prometheus estão no arquivo `/src/prometheus.yml`, ele já está preparado para utilizar o endpoint correto para extrair as métricas.
No arquivo `/src/appsettings.json`.
