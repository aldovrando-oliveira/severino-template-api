# Severino Api Template

Template WebApi com Monitoramento, Log, Notificações e conexão com banco MySql.

## Como instalar

1. Faça o clone no projeto com o comando `git clone https://gitlab.com/aldovrando-oliveira/severino-template-api.git`
2. Na pasta raiz do projeto execute `dotnet new --install .`

## Como usar

1. Após a instalação do template, execute o comando `dotnet new -l` e verifique se há a opção `severinoapi`

![list_templates](docs/images/list_templates.png)

2. Executar os comandos abaixo
```shell
~$ mkdir Novo.Projeto
~$ cd Novo.Projeto
~$ dotnet new severinoapi
```

3. Para verificar os parâmetros adicionais que podem ser utilizados com esse template basta executar o comando `dotnet new severinoapi -h`  

![template_options](docs/images/severinoapi_options.png)

Comando de novo projeto passando parâmetros: `dotnet new severinoapi -A "Lampiao da Silva" -D "Projeto para consulta diversas" -T "Projeto Lampiao"`

## Funcionalidades

### Logger
O template contém utiliza a bliblioteca [NetStash.Core](https://github.com/aldovrando-oliveira/NetStash.Core) que fornece suporte para Log por TCP/IP e também a biblioteca [NetStash.Extensions.Logging](https://github.com/aldovrando-oliveira/NetStash.Extensions.Logging) que implementa o padrão `Microsoft.Extensions.Logging`.

### Documentação das APIs
![swagger](content/docs/images/swagger.png)
Foi utilizada a biblioteca [Swashbuckle.AspNetCore](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) para documentação das APIs com o Swagger


### Monitoramento

![monitoramento](content/docs/images/grafana_prometheus.png)
Foi utilizada biblioteca AppMetrics para extrair as métricas das APIs e o banco TSDB Prometheus para armazenamento. Para visualização é possível é utilizar o Grafana. Há Dashbords no site do GrafanaLabs já preperados para as métricas extraídas por essa biblioteca.

### Integrações
* [Sentry](https://sentry.io/welcome/): O projeto possui interação com sentry, implementadocomo extensão da biblioteca `Microsoft.Extensions.Logging`. Para mais pesquise na [documentação](https://docs.sentry.io/platforms/dotnet/microsoft-extensions-logging/).

### Testes Integradas

Há um exemplo de implementação de testes integrados, utilizando banco de dados em memória.
O teste é feito chamando as APIs e validando as respostas em cada um dos cenários. A implementação dos testes está na pasta `test/`

